﻿namespace AnitableAPI.Model
{
    public class ShowTag
    {
        public int TagId { get; set; }
        public string? Name { get; set; }
    }
}
