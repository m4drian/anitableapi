﻿namespace AnitableAPI.Model
{
    public class Studio
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
