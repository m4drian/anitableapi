﻿namespace AnitableAPI.Model
{
    public class Show
    {
        public int Id { get; set; }

        public string? Title { get; set; }

        public string? Description { get; set; }
        
        
        public ICollection<ShowTag> Tags = new List<ShowTag>();

        public ICollection<Studio> Studios = new List<Studio>();

        //Ongoing, Finished Airing, Upcoming
        public string? ShowStatus { get; set; }

        public DateOnly ReleaseDate { get; set; }

        public int NumberOfEpisodes { get; set; }

        public int PopularityRanking { get; set; } 

        public DateTime NextEpisodeDate { get; set; }

        public string? EpisodeWeekday { get; set; } 

        public TimeOnly EpisodeReleaseHour { get; set; }

        public string? ImageURL { get; set; }

    }
}
